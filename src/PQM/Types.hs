module PQM.Types where

import Control.Monad.Trans.Except
import Control.Monad.State.Lazy

-- CIRCUIT RELATED
type Label = Int 
data WireType = Bit | Qubit deriving (Eq, Show)

type LabelContext = [(Label, WireType)]
showLabelContext :: LabelContext -> String
showLabelContext [] = error "Empty label context"
showLabelContext [(l,a)] = "l" ++ show l ++ " : " ++ show a
showLabelContext ((l,a):qr) = "l" ++ show l ++ " : " ++ show a ++ ", " ++ showLabelContext qr

data BitVal = Zero | One deriving (Eq)
instance Show BitVal where
    show Zero = "0"
    show One = "1"
type Condition = [(Label, BitVal)]
showCondition :: Condition -> String
showCondition [] = "ε"
showCondition [(l,v)] = "l" ++ show l ++ " = " ++ show v
showCondition ((l,v):_) = "l" ++ show l ++ " = " ++ show v ++ " & "

data Gate = H | X | Z | Meas deriving (Eq, Show)
data Circuit
    = CInput LabelContext
    | CCons Circuit Condition Gate LabelContext LabelContext
    | CLift Circuit Condition Label
    deriving (Eq)
instance Show Circuit where
    show (CInput q) = "input(" ++ showLabelContext q ++ ");\n"
    show (CCons c k g q q') = show c
        ++ (if null k then "" else showCondition k ++ " ? ")
        ++ show g ++ "(" ++ showLabelContext q
        ++ ") -> (" ++ showLabelContext q' ++ ");\n"
    show (CLift c k l) = show c
        ++ (if null k then "" else showCondition k ++ " ? ")
        ++ "lift(l" ++ show l ++ ");\n" 


type OutputTree = [(Condition, LabelContext)]

-- TYPE RELATED
type Identifier = String
data Type
    = Wire WireType
    | Tensor Type Type
    | Arrow Type Type
    | Bang Type
    | Circt Type Type
    | TUnit
    deriving (Eq)

instance Show Type where
    show (Wire w) = show w
    show (Tensor a b) = show a ++ " ⊗ " ++ show b
    show (Arrow a b) = show a ++ " ⊸ " ++ show b
    show (Bang a) = "!" ++ show a
    show (Circt t u) = "Circ(" ++ show t ++ ", " ++ show u ++ ")"
    show TUnit = "Unit"

type TypingContext = [(Identifier, Type)]

type TypeError = String
--The following describes a computation that can read/write to typing and label contexts as a side effect
-- i.e. computations in a type inference proof
type Judgement s a = StateT s (Except TypeError) a

-- TERM RELATED
data Term
    = Var Identifier
    | Label Label
    | Abs Identifier Type Term
    | App Term Term
    | Tuple Term Term
    | Letin Identifier Identifier Term Term
    | Lift Term
    | Force Term
    | Box Type Term
    | Apply Term Term
    | Circ Term Circuit Term    --Label vectors are enforced during typing
    | Unit
    | Measas Label Term Term Term
    | Seq Term Term
    deriving (Eq)


instance Show Term where
    show (Var x) = x
    show (Label l) = "l" ++ show l
    show (Abs x a m) = "\\" ++ x ++ " : " ++ show a ++ " -> " ++ show m
    show (App m n) = "(" ++ show m ++ ")(" ++ show n ++ ")"
    show (Tuple m n) = "<" ++ show m ++ ", " ++ show n ++ ">"
    show (Letin x y m n) = "let <" ++ x ++ ", " ++ y ++ "> = " ++ show m ++ " in " ++ show n
    show (Lift m) = "lift(" ++ show m ++ ")"
    show (Force m) = "force(" ++ show m ++ ")"
    show (Box t m) = "box:" ++ show t ++ "(" ++ show m ++ ")"
    show (Apply m n) = "apply(" ++ show m ++ ", " ++ show n ++ ")"
    show (Circ l d l') = "(" ++ show l ++ ", " ++ show d ++ ", " ++ show l' ++ ")"
    show Unit = "*"
    show (Measas l m n p) = "measure " ++ show l ++ " = " ++ show m ++ " as { 0 -> " ++ show n ++ " | 1 -> " ++ show p ++ " }"
    show (Seq m n) = show m ++ "; " ++ show n
    -- MACHINE RELATED

data StackFrame
    = FArg Term
    | FApp Term
    | ALabel Term
    | ACirc Term
    | TRight Term
    | TLeft Term
    | SFBox LabelContext Term
    | Sub Circuit Term Condition
    | Let Identifier Identifier Term
    | SFForce
    | LBranch Label Term Term
    | RBranch Label Term Term
    | Then Term
    deriving (Eq, Show)

type Stack = [StackFrame]

showStack :: Stack -> String
showStack [] = "ε"
showStack [f] = show f 
showStack (f:sr) = show f ++ " · " ++ showStack sr

type Configuration = (Circuit, Term, Stack, Condition)