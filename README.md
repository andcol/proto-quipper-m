# Proto-Quipper-M

## Build instructions

The interpreter is built using the [Stack](https://docs.haskellstack.org/en/stable/README/) build tool. To build, run the following in the root directory.

```
$ stack build
```

## How to use

Currently the interpreter is hardwired to open the file named "test.pqm". Write your program in that file and then run

```
$ stack run
```

The syntax of Proto-Quipper-M is the following:

```
Term	::= Variable | Label | Constant | * | Term;Term
			| \Variable : Type -> Term | Term Term
			| <Term,Term> | let <Variable,Variable> = Term in Term
			| lift Term | force Term
			| box : Type Term | apply(Term,Term)
			| measure Label = Term as { 0 -> Term | 1 -> Term}
```

where variable identifiers are strings of letters (no numbers) starting with a lowercase letter, label identifiers are of the form `ln`, where `n` is a natural number, and constants are currently one of `H, X, Z, Meas`. Type annotations are mandatory and they are defined by the following grammar:

```
Type	::= Qubit | Bit | Unit | Type -o Type | Type * Type
			| !Type | Circ(Type,Type)
```

