module PQM.Circuit where

import PQM.Types
import Control.Monad
import Control.Monad.Except
import Control.Monad.State.Lazy
import Data.List

gateInputs :: Gate -> Type
gateInputs H = Wire Qubit
gateInputs X = Wire Qubit
gateInputs Z = Wire Qubit
gateInputs Meas = Wire Qubit

gateOutputs :: Gate -> Type
gateOutputs H = Wire Qubit
gateOutputs X = Wire Qubit
gateOutputs Z = Wire Qubit
gateOutputs Meas = Wire Bit

typeOf :: LabelContext -> Type
typeOf [] = error "Tried to get type of empty context (this should not be possible)"
typeOf [(l,w)] = Wire w
typeOf ((l,w):lcr) = Tensor (Wire w) (typeOf lcr)

subsetOf :: Eq a => [a] -> [a] -> Bool
subsetOf l1 l2 = all (`elem` l2) l1

freshIn :: Eq a => [a] -> [a] -> Bool
freshIn l1 l2 = l2 \\ l1 == l2

updateBranch :: Condition -> OutputTree -> LabelContext -> OutputTree
updateBranch k ((k',lcold):br) lcnew | k == k' = (k',lcnew):br
updateBranch k ((k',lcold):br) lcnew = (k',lcold) : updateBranch k br lcnew

consume :: LabelContext -> Condition -> Judgement (LabelContext,OutputTree) ()
consume lc k = do
    (lci, b) <- get
    case lookup k b of
        Just lco -> do
            unless (lc `subsetOf` lco) (throwError "Tried to consume undefined labels")
            put (lci, updateBranch k b (lco \\ lc))
        Nothing -> throwError "Target branch does not exist"

introduce :: LabelContext -> Condition -> Judgement (LabelContext,OutputTree) ()
introduce lc k = do
    (lci, b) <- get
    case lookup k b of
        Just lco -> do
            unless (lc `freshIn` lco) (throwError "Tried to introduce existing labels")
            put (lci, updateBranch k b (lco ++ lc))
        Nothing -> throwError "Target branch does not exist"

judge :: Circuit -> Judgement (LabelContext,OutputTree) ()
judge (CInput lc) = put (lc, [([],lc)])
judge (CCons c k g lci lco) = do
    judge c
    consume lci k
    introduce lco k
judge (CLift c k l) = do
    judge c
    (lci, b) <- get
    case lookup k b of
        Just lco -> let b' = ((l,Zero):k, lco):((l,One):k, lco):(b \\ [(k,lco)]) in
            put (lci, b')
        Nothing -> throwError "Target branch does not exist"

converges :: LabelContext -> OutputTree -> Bool
converges lc = all ((==lc) . snd)

inferType :: Circuit -> Either TypeError (LabelContext, LabelContext)
inferType c = do
    (lci, b) <- runExcept $ execStateT (judge c) ([],[])
    let tentativeOutput = snd $ head b
    unless (converges tentativeOutput b) (throwError "Circuit has no converging output type")
    return (lci, tentativeOutput)

greatestLabel :: Circuit -> Label
greatestLabel (CInput q) = maximum $ map fst q
greatestLabel (CCons c _ _ q l) = maximum (greatestLabel c : map fst q ++ map fst l)
greatestLabel (CLift c _ _) = greatestLabel c

getLabelNames :: Circuit -> [Label]
getLabelNames (CInput q) = map fst q
getLabelNames (CCons c _ _ q l) = getLabelNames c ++ map fst q ++ map fst l
getLabelNames (CLift c _ _) = getLabelNames c

makeFresh :: Circuit -> Int -> [Label]
makeFresh c n = map (greatestLabel c +) [1..n]

rename :: [Label] -> [Label] -> Circuit -> Circuit
rename [] [] c = c
rename (l:lr) (l':lr') c = rename lr lr' $ rename' l l' c
    where
        rename' :: Label -> Label -> Circuit -> Circuit
        rename' l l' (CInput q) = CInput (renameInMap l l' q)
        rename' l l' (CCons c k g q q') = let new = renameInMap l l' in
            CCons (rename' l l' c) (new k) g (new q) (new q')
        rename' l l' (CLift c k l'') | l == l'' = CLift (rename' l l' c) (renameInMap l l' k) l'
        rename' l l' (CLift c k l'') = CLift (rename' l l' c) (renameInMap l l' k) l''

        renameInMap :: Label -> Label -> [(Label, a)] -> [(Label, a)]
        renameInMap l l' [] = []
        renameInMap l l' ((k,a) : qr) | l == k = (l', a) : qr
        renameInMap l l' ((k,a) : qr) = (k, a) : renameInMap l l' qr

cconcat :: Circuit -> Circuit -> Circuit
cconcat c (CInput q) = c
cconcat c (CCons d k g q q') = CCons (cconcat c d) k g q q'
cconcat c (CLift d k l) = CLift (cconcat c d) k l
