module PQM.Term where

import PQM.Types

isValue :: Term -> Bool
isValue (Label _) = True
isValue Abs {} = True
isValue (Tuple l r) = isValue l && isValue r
isValue (Lift _) = True
isValue Circ {} = True
isValue Unit = True
isValue _ = False

isLabelVector :: Term -> Bool
isLabelVector (Label _) = True
isLabelVector (Tuple m n) = isLabelVector m && isLabelVector n
isLabelVector _ = False

greatestLabel :: Term -> Label
greatestLabel (Var x) = 0
greatestLabel Unit = 0
greatestLabel (Label l) = l
greatestLabel (App m n) = max (greatestLabel m) (greatestLabel n)
greatestLabel (Abs x a m) = greatestLabel m
greatestLabel (Tuple m n) = max (greatestLabel m) (greatestLabel n)
greatestLabel (Letin y z m n) = max (greatestLabel m) (greatestLabel n)
greatestLabel (Lift m) = greatestLabel m
greatestLabel (Force m) = greatestLabel m
greatestLabel (Box t m) = greatestLabel m
greatestLabel (Apply m n) = max (greatestLabel m) (greatestLabel n)
greatestLabel (Circ l d l') = max (greatestLabel l) (greatestLabel l')
greatestLabel (Measas l m n p) = maximum [l, greatestLabel m, greatestLabel n, greatestLabel p]
greatestLabel (Seq m n) = max (greatestLabel m) (greatestLabel n)

getLabelNames :: Term -> [Label]
getLabelNames (Var x) = []
getLabelNames Unit = []
getLabelNames (Label l) = [l]
getLabelNames (App m n) = getLabelNames m ++ getLabelNames n
getLabelNames (Abs x a m) = getLabelNames m
getLabelNames (Tuple m n) = getLabelNames m ++ getLabelNames n
getLabelNames (Letin _ _ m n) = getLabelNames m ++ getLabelNames n
getLabelNames (Lift m) = getLabelNames m
getLabelNames (Force m) = getLabelNames m
getLabelNames (Box _ m) = getLabelNames m
getLabelNames (Apply m n) = getLabelNames m ++ getLabelNames n
getLabelNames (Circ l _ l') = getLabelNames l ++ getLabelNames l'
getLabelNames (Measas l m n p) = l : getLabelNames m ++ getLabelNames n ++ getLabelNames p
getLabelNames (Seq m n) = getLabelNames m ++ getLabelNames n

fromLabelNames :: [Label] -> Term
fromLabelNames [] = error "Cannot get a term from an empty list"
fromLabelNames [l] = Label l
fromLabelNames (l:lr) = Label l `Tuple` fromLabelNames lr