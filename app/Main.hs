module Main where

import Text.Parsec
import PQM.Parsing
import PQM.Machine
import PQM.Types
import PQM.Circuit

main :: IO ()
main = do
    let filename = "test.pqm"
    programfile <- readFile filename
    --parseTest parseProgram programfile
    case parse parseProgram filename programfile of
        Left parseError -> print parseError
        Right term -> do
            putStrLn "\n----PROGRAM----\n"
            print term
            case run (CInput [(0,Qubit),(1,Qubit),(2,Qubit),(3,Qubit)]) term of
                Right (c, v) -> do
                    putStrLn "\n----CIRCUIT----\n"
                    print c
                    putStrLn "---------------"
                    putStrLn $ "VALUE : " ++ show v
                Left (err, (c,m,s,k)) -> do
                    putStrLn "\n----ERROR----\n"
                    putStrLn $ "CAUSE\t| " ++ err
                    putStrLn $ "TERM\t| " ++ show m
                    putStrLn $ "STACK\t| " ++ showStack s
                    putStrLn $ "COND.\t| " ++ showCondition k