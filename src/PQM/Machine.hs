module PQM.Machine where

import PQM.Types
import PQM.Term
import PQM.TypeSystem
import PQM.Circuit
import Data.List

sub :: Term -> Identifier -> Term -> Term
sub n x (Var y) | x == y = n
sub n x (Var y) = Var y
sub n x (Label l) = Label l
sub n x (Abs y a m) | x == y = Abs y a m
sub n x (Abs y a m) = Abs y a (sub n x m)
sub n x (Tuple m p) = Tuple (sub n x m) (sub n x p)
sub n x (Letin y z m p) | x == y || x == z = Letin y z (sub n x m) p
sub n x (Letin y z m p) = Letin y z (sub n x m) (sub n x p)
sub n x (Lift m) = Lift (sub n x m)
sub n x (Force m) = Force (sub n x m)
sub n x (Box t m) = Box t (sub n x m)
sub n x (Apply m p) = Apply (sub n x m) (sub n x p)
sub n x (Circ l d l') = Circ l d l'
sub n x Unit = Unit

append :: Circuit -> Condition -> Term -> Term -> Circuit -> Term -> (Circuit, Term)
append c h k l d l' = let   k' = makeFresh c (length $ PQM.Term.getLabelNames l')
                            d' = rename (PQM.Term.getLabelNames l ++ PQM.Term.getLabelNames l') (PQM.Term.getLabelNames k ++ k') d
                            d'' = paste h d'
                            in (cconcat c d'', fromLabelNames k')
                            where
                                paste :: Condition -> Circuit -> Circuit
                                paste h (CInput q) = CInput q
                                paste h (CCons c k g q q') = CCons c (h ++ k) g q q'
                                paste h (CLift c k l) = CLift c (h ++ k) l

freshLabels :: Term -> Type -> (LabelContext, Term)
freshLabels m t | isMType t =
    let firstLabel = PQM.Term.greatestLabel m + 1 in makeLabels firstLabel t
        where
            makeLabels l (Wire a) = ([(l, a)], Label l)
            makeLabels l (Tensor t u) =
                let (q', l') = makeLabels l t
                    (q'', l'') = makeLabels (PQM.Term.greatestLabel l' + 1) u
                    in (q' ++ q'', Tuple l' l'') --TEST this

type RuntimeError = (String, Configuration)
                    
step :: Configuration -> Either RuntimeError Configuration
step (c, App m n, s, k) = Right (c, m, FArg n : s, k)                         --app-split
step (c, v, FArg n : s, k) | isValue v = Right (c, n, FApp v : s, k)          --app-shift
step (c, v, FApp (Abs x _ m) : s, k) | isValue v = Right (c, sub v x m, s, k) --app-join
step (c, v, FApp x : s, k) | isValue v =
    Left ("Expected application on stack", (c, v, FApp x : s, k))
step (c, Apply m n, s, k) = Right (c, m, ALabel n : s, k)                     --apply-split
step (c, v, ALabel n : s, k) | isValue v = Right (c, n, ACirc v : s, k)       --apply-shift
step (c, k, ACirc (Circ l d l') : s, h) | isLabelVector k =
    let (c', k') = append c h k l d l' in Right (c', k', s, h)                --apply-join 
step (c, k, ACirc x : s, h) | isLabelVector k =
    Left ("Expected circuit on stack", (c, k, ACirc x : s, h)) 
step (c, v, ACirc (Circ l d l') : s, h) | isValue v=
    Left ("Expected label vector term", (c, v, ACirc (Circ l d l') : s, h))
step (c, v, ACirc y : s, h) | isValue v =
    Left ("Expected circuit on the stack and label vector term", (c, v, ACirc y : s, h)) 
step (c, Tuple m n, s, k) | not . isValue $ Tuple m n =
    Right (c, m, TRight n : s, k)                                             --tuple-split
step (c, v, TRight n : s, k) | isValue v = Right (c, n, TLeft v : s, k)       --tuple-shift
step (c, w, TLeft v : s, k) | isValue v && isValue w =
    Right (c, Tuple v w, s, k)                                                --tuple-join
step (c, Box t m, s, k) =
    let (q, l) = freshLabels m t in Right (c, m, SFBox q l : s, k)            --box-open
step (c, Lift m, SFBox q l : s, k) =
    Right (CInput q, App m l, Sub c l k : s, [])                              --box-sub
step (c, v, SFBox q l : s, k) | isValue v =
    Left ("Expected lifted term", (c, v, SFBox q l : s, k))
step (d, l', Sub c l h : s, k) | isLabelVector l' =
    Right (c, Circ l d l', s, h)                                              --box-close
step (d, v, Sub c l h : s, k) | isValue v =
    Left ("Expected label vector term", (d, v, Sub c l h : s, k))
step (c, Letin x y m n, s, k) = Right (c, m, Let x y n : s, k)                --let-split
step (c, Tuple v w, Let x y n : s, k) | isValue (Tuple v w) =
    Right (c, sub w y (sub v x n), s, k)                                      --let-join
step (c, v, Let x y n : s, k) | isValue v =
    Left ("Expected tuple term", (c, v, Let x y n : s, k))
step (c, Force m, s, k) = Right (c, m, SFForce : s, k)                        --force-open
step (c, Lift m, SFForce : s, k) = Right (c, m, s, k)                         --force-close
step (c, v, SFForce : s, k) | isValue v =
    Left ("Expected lifted term", (c, v, SFForce : s, k))
step (c, Measas l m n p, s, k) = Right (c, m, LBranch l n p : s, k)           --meas-split
step (c, Label l', LBranch l n p : s, k) =
    let c' = CLift (CCons c k Meas [(l',Qubit)] [(l,Bit)]) k l in
        Right (c', n, s ++ RBranch l n p : s, (l,Zero):k)                     --meas-zero
step (c, v, LBranch l n p : s, k) | isValue v =
    Left ("Expected label", (c, v, LBranch l n p : s, k))
step (c, Unit, RBranch l n p : s, k) | (l,Zero) `elem` k =
    Right (c, p, s, (l,One):(k \\ [(l,Zero)]))                                --meas-one
step (c, v, RBranch l n p : s, k) | (l,Zero) `elem` k && isValue v =
    Left ("Expected * term", (c, v, RBranch l n p : s, k))
step (c, Unit, RBranch l n p : s, k) =
    Left ("Expected l=0 in condition", (c, Unit, RBranch l n p : s, k))
step (c, v, RBranch l n p : s, k) | isValue v =
    Left ("Expected * term and l=0 in condition", (c, v, RBranch l n p : s, k))
step (c, Seq m n, s, k) = Right (c, m, Then n : s, k)                         --seq-split
step (c, v, Then n : s, k) | isValue v = Right (c, n, s, k)                   --seq-shift
step x = Left ("Cannot reduce configuration", x)

load :: Circuit -> Term -> Configuration
load c m = (c, m, [], [])

normalize :: Configuration -> Either RuntimeError (Circuit, Term)
normalize (c, v, [], k) | isValue v = Right (c,v)
normalize c = case step c of
    Right (circ, val, [], []) | isValue val -> Right (circ, val)
    Right c' -> normalize c'
    Left err -> Left err

run :: Circuit -> Term -> Either RuntimeError (Circuit, Term)
run c m = normalize $ load c m

