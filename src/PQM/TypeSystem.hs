module PQM.TypeSystem where

import PQM.Types
import Control.Monad
import Control.Monad.Except
import Control.Monad.State.Lazy
import Data.List
import PQM.Circuit
import PQM.Term

isLinear :: Type -> Bool
isLinear (Wire _) = True
isLinear (Tensor a b) = isLinear a && isLinear b
isLinear (Arrow _ _) = True
isLinear _ = False

isMType :: Type -> Bool
isMType (Wire _) = True
isMType (Tensor a b) = isMType a && isMType b
isMType _ = False

isParameter :: Type -> Bool
isParameter = not . isLinear

isParameterContext :: TypingContext -> Bool
isParameterContext = all (isParameter . snd)

varLookup :: Identifier -> Judgement (TypingContext , LabelContext) Type
varLookup x = do
    (tc, lc) <- get
    case varLookup' tc x of
        Just (a, tc') -> put (tc', lc) >> return a
        Nothing -> throwError $ "Undefined variable " ++ show x

varLookup' :: TypingContext -> Identifier -> Maybe (Type, TypingContext)
varLookup' [] _ = Nothing
varLookup' ((x,a):tcr) y | x == y = if isLinear a then Just (a, tcr) else Just (a, (x,a):tcr)
varLookup' ((x,a):tcr) y = do
    (b, tcr') <- varLookup' tcr y
    return (b, (x,a):tcr')

labelLookup :: Label -> Judgement (TypingContext , LabelContext) Type
labelLookup l = do
    (tc, lc) <- get
    case labelLookup' lc l of
        Just (w, lc') -> put (tc, lc') >> return (Wire w)
        Nothing -> throwError $ "Undefined label " ++ show l

labelLookup' :: LabelContext  -> Label -> Maybe (WireType , LabelContext)
labelLookup' [] _ = Nothing
labelLookup' ((l,w):lcr) k | l == k = Just (w, lcr)
labelLookup' ((l,w):lcr) k = do
    (w', lcr') <- labelLookup' lcr k
    return (w', (l,w):lcr')

bind :: Identifier -> Type -> Judgement (TypingContext , LabelContext) ()
bind x a = do
    (tc, lc) <- get
    put ((x,a):tc, lc)

defined :: Identifier -> Judgement (TypingContext , LabelContext) Bool
defined x = do
    (tc, _) <- get
    let tcdom = map fst tc
    return (x `elem` tcdom)

judge' :: Term -> Judgement (TypingContext , LabelContext) Type
judge' (Var x) = varLookup x
judge' (Label l) = labelLookup l
judge' (Abs x a m) = do
    bind x a
    b <- judge' m
    usedArg <-  not <$> defined "x"
    unless usedArg (throwError $ "Unused var " ++ x)
    return $ Arrow a b
judge' (App m n) = do
    a <- judge' m
    b <- judge' n
    case a of
        Arrow c d -> do
            unless (b == c) (throwError $ "Cannot apply function of type " ++ show (Arrow c d) ++ " to argument of type " ++ show b)
            return d
        a -> throwError $ "Expected function, got term of type " ++ show a
judge' (Tuple m n) = do
    a <- judge' m
    b <- judge' n
    return $ Tensor a b
judge' (Letin x y m n) = do
    a <- judge' m
    case a of
        Tensor b c -> do
            bind x b
            bind y c
            judge' n
        a -> throwError $ "Expected tuple, got term of type " ++ show a
judge' (Lift m) = do
    (tc, lc) <- get
    a <- judge' m
    (tc', lc') <- get
    unless (isParameterContext (tc\\tc') && null (lc\\lc')) (throwError "Linear resources in lifted term")
    return $ Bang a
judge' (Force m) = do
    a <- judge' m
    case a of
        Bang b -> return b
        a -> throwError $ "Expected lifted term, got term of type " ++ show a
judge' (Box t m) = do
    unless (isMType t) (throwError $ "Invalid type annotation " ++ show t ++ " in box")
    a <- judge' m
    case a of
        Bang (Arrow t' u) -> do
            unless (isMType u) (throwError $ "Cannot box a function which returns type " ++ show u)
            unless (t == t') (throwError $ "Type mismatch between type annotation" ++ show t ++ " and boxed function of type " ++ show t' ++ "->" ++ show u)
            return $ Circt t u
        a -> throwError $ "Expected lifted function, got term of type " ++ show a
judge' (Apply m n) = do
    a <- judge' m
    case a of
        Circt t u -> do
            unless (isMType t && isMType u) (throwError "Unexpected error in circuit")
            t' <- judge' n
            unless (t == t') (throwError $ "Cannot apply circuit of type " ++ show (Circt t u) ++ " to label vector of type " ++ show u)
            return u
        a -> throwError $ "Expected circuit, got term of type " ++ show a
judge' (Circ vli c vlo) = do
    unless (isLabelVector vli && isLabelVector vlo) (throwError "Expected s label vector, got a generic term instead")
    backup <- get
    case PQM.Circuit.inferType c of
        Right (lci, lco) -> do
            put ([], lci)
            a <- judge' vli
            put ([], lco)
            b <- judge' vlo
            unless (isMType a && isMType b) (throwError "Expected MType, got generic type instead")
            put backup
            return $ Circt a b
        Left e -> throwError $ "Circuit is malformed: " ++ e
judge' Unit = return TUnit


judge :: Term -> Judgement (TypingContext , LabelContext) Type
judge m = do
    a <- judge' m
    (tc, _) <- get
    unless (isParameterContext tc) (throwError $ "Unused linear resources in " ++ show tc)
    return a

inferType :: LabelContext -> Term -> Either TypeError Type
inferType lc m = do
    runExcept $ evalStateT (PQM.TypeSystem.judge m) ([],lc)
