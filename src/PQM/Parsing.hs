module PQM.Parsing where

import Text.Parsec
import PQM.Types
import Debug.Trace
import Control.Monad

type TermParser = Parsec String () Term

reservedKeyWords :: [String]
reservedKeyWords = ["let", "in", "measure", "as", "box", "lift", "force", "apply"]

parseProgram :: TermParser
parseProgram = do
    p <- parseTerm
    eof
    return p

parseTerm :: TermParser
parseTerm = chainl1 parseNonSeq (spaces >> char ';' >> spaces >> return Seq)

parseNonSeq :: TermParser
parseNonSeq = chainl1 parseTermNonRec (spaces >> return App)

parseTermNonRec :: TermParser
parseTermNonRec = choice [
    parseAbs <?> "abstraction",
    parseTuple <?> "tuple",
    parseUnit <?> "unit",
    try parseLetin <?> "let expression",
    try parseLift <?> "lift expression",
    try parseForce <?> "force expression",
    try parseBox <?> "box expression",
    try parseApply <?> "apply",
    try parseMeas <?> "measure expression",
    parseLabel,
    try parseVar,
    parseConst,
    between (char '(') (char ')') parseTerm
    ]

variableId :: Parsec String () String
variableId = (:) <$> lower <*> many letter 

constId :: Parsec String () String
constId = (:) <$> upper <*> many letter

numberId :: Parsec String () Int
numberId = do
    n <- choice [string "0", oneOf ['1'..'9'] >>= \n' -> many digit >>= \n'' -> return (n' : n'')] 
    return $ read n

parseVar :: TermParser
parseVar = do
    id <- variableId
    when (id `elem` reservedKeyWords)
        (unexpected $ "Reserved keyword " ++ id ++ " cannot be an identifier")
    spaces
    return $ Var id

parseConst :: TermParser
parseConst = do
    id <- constId
    case id of
        "H" -> return $ Circ (Label 0) (CCons (CInput [(0, Qubit)]) [] H [(0, Qubit)] [(1, Qubit)]) (Label 1)
        "X" -> return $ Circ (Label 0) (CCons (CInput [(0, Qubit)]) [] X [(0, Qubit)] [(1, Qubit)]) (Label 1)
        "Z" -> return $ Circ (Label 0) (CCons (CInput [(0, Qubit)]) [] Z [(0, Qubit)] [(1, Qubit)]) (Label 1)
        "Meas" -> return $ Circ (Label 0) (CCons (CInput [(0, Qubit)]) [] Meas [(0, Qubit)] [(1, Bit)]) (Label 1)
        _ -> unexpected "Expected one of H, X, Z or Meas"

parseUnit :: TermParser
parseUnit = char '*' >> spaces >> return Unit

parseLabel :: TermParser
parseLabel = do
    char 'l'
    n <- numberId <?> "number"
    spaces
    return $ Label n

parseAbs :: TermParser
parseAbs = do
    char '\\'
    id <- variableId
    spaces >> char ':' >> spaces
    a <- parseType
    spaces >> string "->" >> spaces
    m <- parseTerm
    spaces
    return $ Abs id a m

parseTuple :: TermParser
parseTuple = do
    char '<' >> spaces
    m <- parseTerm
    spaces >> char ',' >> spaces
    n <- parseTerm
    spaces >> char '>' >> spaces
    return $ Tuple m n

parseLetin :: TermParser
parseLetin = do
    string "let" >> spaces >> char '<' >> spaces
    x <- variableId
    spaces >> char ',' >> spaces
    y <- variableId
    spaces >> char '>' >> spaces >> char '=' >> spaces
    m <- parseTerm
    spaces >> string "in" >> spaces
    n <- parseTerm
    spaces
    return $ Letin x y m n

parseLift :: TermParser
parseLift = do
    string "lift" >> spaces
    m <- parseTerm
    spaces
    return $ Lift m

parseForce :: TermParser
parseForce = do
    string "force" >> spaces
    m <- parseTerm
    spaces
    return $ Force m

parseBox :: TermParser
parseBox = do
    string "box" >> spaces >> char ':' >> spaces
    t <- parseType
    spaces
    m <- parseTerm
    spaces
    return $ Box t m

parseApply :: TermParser
parseApply = do
    string "apply" >> spaces >> char '(' >> spaces
    m <- parseTerm
    spaces >> char ',' >> spaces
    n <- parseTerm
    spaces >> char ')' >> spaces
    return $ Apply m n

parseMeas :: TermParser
parseMeas = do
    string "measure" >> spaces
    l <- char 'l' >> numberId
    spaces >> char '=' >> spaces
    m <- parseTerm
    spaces >> string "as" >> spaces >> char '{'
    spaces >> char '0' >> spaces >> string "->" >> spaces
    n <- parseTerm
    spaces >> char '|' >> spaces >> char '1' >> spaces >> string "->" >> spaces
    p <- parseTerm
    spaces >> char '}'
    return $ Measas l m n p

-- TYPES

type TypeParser = Parsec String () Type

parseType :: TypeParser
parseType = chainl1 parseNonArrow (try $ spaces >> string "-o" >> spaces >> return Arrow) 

parseNonArrow :: TypeParser
parseNonArrow = chainr1 parseNonRec (spaces >> char '*' >> spaces >> return Tensor)

parseNonRec :: TypeParser
parseNonRec = choice [
    parseWire,
    parseBang,
    parseCirct,
    between (char '(') (char ')') parseType
    ]

parseWire :: TypeParser
parseWire = do
    w <- string "Bit" <|> string "Qubit"
    spaces
    case w of
        "Bit" -> return $ Wire Bit
        "Qubit" -> return $ Wire Qubit
        _ -> error "this should never happen"

parseBang :: TypeParser
parseBang = do
    char '!' >> spaces
    t <- parseType
    spaces
    return $ Bang t

parseCirct :: TypeParser
parseCirct = do
    string "Circ" >> spaces >> char '('
    t <- parseType
    spaces >> char ',' >> spaces
    u <- parseType
    spaces >> char ')' >> spaces
    return $ Circt t u

parseTUnit :: TypeParser
parseTUnit = string "Unit" >> return TUnit

